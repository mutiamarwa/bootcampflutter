import 'dart:io';

void main() {
//No.1 start
  var word = 'dart';
  var second = 'is';
  var third = 'awesome';
  var fourth = 'and';
  var fifth = 'I';
  var sixth = 'love';
  var seventh = 'it!';

  print(word +
      ' ' +
      second +
      ' ' +
      third +
      ' ' +
      fourth +
      ' ' +
      fifth +
      ' ' +
      sixth +
      ' ' +
      seventh);
//no.1 finish

//No.2 start
  var sentence = "I am going to be Flutter Developer";
  var exampleFirstWord = sentence[0];
  var exampleSecondWord = sentence[2] + sentence[3];
  var thirdWord = sentence.substring(5, 10);
  var fourthWord = sentence.substring(11, 13);
  var fifthWord = sentence.substring(14, 16);
  var sixthWord = sentence.substring(17, 24);
  var seventhWord = sentence.substring(25, 34);

  print('First Word: ' + exampleFirstWord);
  print('Second Word: ' + exampleSecondWord);
  print('Third Word: ' + thirdWord);
  print('Fourth Word: ' + fourthWord);
  print('Fifth Word: ' + fifthWord);
  print('Sixth Word: ' + sixthWord);
  print('Seventh Word: ' + seventhWord);
//no.2 finish

//No.3 start
  print("masukan nama depan :");
  String namaDepan = stdin.readLineSync()!;
  print("masukan nama belakang :");
  String namaBelakang = stdin.readLineSync()!;
  print("nama lengkap anda adalah:");
  print(namaDepan + ' ' + namaBelakang);
//no.3 finish

//No.4
  var a = 5;
  var b = 10;

  print('perkalian : ${a*b}');
  print('pembagian : ${a/b}');
  print('penambahan : ${a+b}');
  print('pengurangan : ${a-b}');
//no.4 finish

}
