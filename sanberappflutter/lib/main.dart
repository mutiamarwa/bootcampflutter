import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sanberappflutter/Latihan/Latihan17/pages/page_1.dart';
import 'package:sanberappflutter/Latihan/Latihan17/routes/page_routes.dart';

//import 'Latihan/Latihan16/pages/get_data_screen.dart';
//import 'package:sanberappflutter/Tugas/Tugas14/get_data.dart';
//import 'package:sanberappflutter/Tugas/Tugas12/HomeScreen.dart';
//import 'package:sanberappflutter/Tugas/Tugas12/LoginScreen.dart';
//import 'package:sanberappflutter/Tugas/Tugas13/routes.dart';
//import 'Tugas/Tugas11/Telegram.dart';
//import 'Tugas/Tugas14/get_data.dart';
// import 'Tugas/Tugas15/LoginScreen.dart';
// import 'Tugas/Tugas15/routes.dart';
// import 'Quiz3/screen/login/login_screen.dart';
// import 'Quiz3/screen/home/home_screen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      //home: const HomeScreen(),
      //home: const LoginScreen(),
      //home: GetDataScreen(),
      // initialRoute: LoginScreen.routeName,
      // routes: routes,
      //home: LoginScreen(),
      //home: Homescreen(),
      //home: GetDataScreen(),
      home: PageOne(),
      getPages: pageRouteApp.pages,
    );
  }
}
