import 'package:flutter/widgets.dart';
import 'package:sanberappflutter/Tugas/Tugas15/HomeScreen.dart';
import 'package:sanberappflutter/Tugas/Tugas15/LoginScreen.dart';
import 'package:sanberappflutter/Tugas/Tugas15/main_page.dart';

final Map<String, WidgetBuilder> routes = {
  LoginScreen.routeName: (context) => LoginScreen(),
  HomeScreen.routeName: (context) => HomeScreen(),
  MainPage.routeName: (context) => MainPage(),
};
