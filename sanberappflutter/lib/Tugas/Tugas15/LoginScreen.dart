import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:sanberappflutter/Tugas/Tugas15/main_page.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);
  static String routeName = "/login";

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  String errorMsg = '';
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    User? user = FirebaseAuth.instance.currentUser;
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            const SizedBox(
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.all(8),
              child: Image.asset(
                "assets/img/logo-dark1.png",
                height: 120,
                width: 120,
              ),
            ),
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(10),
              child: Text(
                "TITIAN",
                style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w800,
                    color: Color(0xff3e5089)),
              ),
            ),
            Form(
              key: _key,
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(10),
                    child: TextFormField(
                      controller: _emailController,
                      validator: validateEmail,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: "Email",
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: TextFormField(
                      obscureText: true,
                      controller: _passwordController,
                      validator: validatePassword,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: "Password",
                      ),
                    ),
                  ),
                  Center(
                    child: Text(errorMsg,
                        style: TextStyle(fontSize: 8, color: Colors.red)),
                  ),
                  TextButton(
                    onPressed: () {},
                    child: Text("Forgot Password"),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 50,
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: ElevatedButton(
                      style: raisedButtonStyle_dark,
                      child: Text("Login"),
                      // onPressed: () async {
                      //   //Navigator.pushNamed(context, MainPage.routeName);
                      //   await _firebaseAuth
                      //       .signInWithEmailAndPassword(
                      //           email: _emailController.text,
                      //           password: _passwordController.text)
                      //       .then((value) =>
                      //           Navigator.pushNamed(context, MainPage.routeName));
                      // },
                      onPressed: () async {
                        if (_key.currentState!.validate()) {
                          try {
                            await _firebaseAuth
                                .signInWithEmailAndPassword(
                                    email: _emailController.text,
                                    password: _passwordController.text)
                                .then((value) => Navigator.of(context)
                                    .pushReplacement(MaterialPageRoute(
                                        builder: (context) => MainPage())));
                            errorMsg = '';
                          } on FirebaseAuthException catch (error) {
                            errorMsg = error.message!;
                          }
                          setState(() {});
                        }
                      },
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(child: Divider()),
                      Text("   OR   ",
                          style:
                              TextStyle(color: Colors.grey[400], fontSize: 10)),
                      Expanded(child: Divider()),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 50,
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: ElevatedButton.icon(
                      icon: Icon(
                        Icons.how_to_reg,
                        color: Color(0xff3e5089),
                      ),
                      // icon: Image.asset(
                      //   "assets/img/google.png",
                      //   height: 15,
                      //   width: 15,
                      // ),
                      style: raisedButtonStyle_light,
                      label: Text("Register",
                          style: TextStyle(color: Color(0xff3e5089))),
                      onPressed: () async {
                        if (_key.currentState!.validate()) {
                          try {
                            await _firebaseAuth
                                .createUserWithEmailAndPassword(
                                    email: _emailController.text,
                                    password: _passwordController.text)
                                .then((value) => Navigator.of(context)
                                    .pushReplacement(MaterialPageRoute(
                                        builder: (context) => MainPage())));
                            errorMsg = '';
                          } on FirebaseAuthException catch (error) {
                            errorMsg = error.message!;
                          }
                          setState(() {});
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
            // Container(
            //   padding: EdgeInsets.all(10),
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.center,
            //     children: <Widget>[
            //       Text("Don't have an account?"),
            //       TextButton(
            //         onPressed: () {},
            //         child: Text(
            //           "Register",
            //           style: TextStyle(fontSize: 15),
            //         ),
            //       ),
            //     ],
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}

final ButtonStyle raisedButtonStyle_dark = ElevatedButton.styleFrom(
  onPrimary: Colors.grey[300],
  primary: Color(0xff3e5089),
  //minimumSize: Size(88, 36),
  minimumSize: const Size.fromHeight(50),
  padding: EdgeInsets.symmetric(horizontal: 16),

  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(6)),
  ),
);

final ButtonStyle raisedButtonStyle_light = ElevatedButton.styleFrom(
  onPrimary: Color(0xff3e5089),
  primary: Colors.grey[300],
  //minimumSize: Size(88, 36),
  minimumSize: const Size.fromHeight(50),
  padding: EdgeInsets.symmetric(horizontal: 16),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(6)),
  ),
);

String? validateEmail(String? formEmail) {
  if (formEmail == null || formEmail.isEmpty)
    return 'Email address is required';

  String pattern = r'\w+@\w+\.\w+';
  RegExp regex = RegExp(pattern);
  if (!regex.hasMatch(formEmail)) return 'Invalid Email address format';

  return null;
}

String? validatePassword(String? formPassword) {
  if (formPassword == null || formPassword.isEmpty)
    return 'Password is required';

  String pattern = r'^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$';
  RegExp regex = RegExp(pattern);
  if (!regex.hasMatch(formPassword))
    return '''Min. 8 charactes, at least one letter and one number''';

  return null;
}
