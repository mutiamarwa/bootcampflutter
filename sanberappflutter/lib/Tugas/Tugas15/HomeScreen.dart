import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:sanberappflutter/Tugas/Tugas12/LoginScreen.dart';

import '../Tugas13/DrawerScreen.dart';

class HomeScreen extends StatelessWidget {
  static String routeName = "/home_screen";
  const HomeScreen({Key? key}) : super(key: key);
  //GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  Future<void> _signOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    FirebaseAuth auth = FirebaseAuth.instance;
    if (auth.currentUser != null) {
      //String _email = auth.currentUser!.email!;
    }
    return Scaffold(
      //backgroundColor: Color(0xfffbfbfd),
      //key: _globalKey,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: Builder(builder: (context) {
          return IconButton(
            onPressed: () {
              //_globalKey.currentState?.openDrawer();
              Scaffold.of(context).openDrawer();
            },
            tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            icon: Icon(Icons.menu),
            color: Colors.grey,
          );
        }),
      ),
      drawer: DrawerScreen(),
      body: Center(
        child: Container(
          padding: EdgeInsets.fromLTRB(16, 0, 16, 16),
          //margin: EdgeInsets.only(top: 40),
          color: Colors.white,
          child: ListView(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Welcome,",
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.w800),
                  ),
                  Icon(
                    Icons.notifications,
                    color: Colors.grey,
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  auth.currentUser!.email!,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w800),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Container(
                height: 50,
                width: double.infinity,
                padding: EdgeInsets.all(13),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: Colors.grey)),
                child: TextFormField(
                  decoration: InputDecoration.collapsed(
                      hintText: "Search jobs or position"),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Jobs For You",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
                ),
              ),
              jobs_item(context),
              Container(
                child: ElevatedButton(
                  // onPressed: () {
                  //   _signOut().then((value) =>
                  //       Navigator.pushNamed(context, LoginScreen.routeName));
                  // },
                  onPressed: () {
                    _signOut().then((value) => Navigator.of(context)
                        .pushReplacement(MaterialPageRoute(
                            builder: (context) => LoginScreen())));
                  },
                  child: Text("Logout"),
                  style: raisedButtonStyle_dark,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

Container jobs_item(BuildContext context) {
  return Container(
    height: MediaQuery.of(context).size.height / 2.2,
    child: GridView.count(
      primary: false,
      padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      crossAxisCount: 2,
      children: <Widget>[
        Container(
          padding: const EdgeInsets.all(8),
          decoration: BoxDecoration(
              color: Color(0xffe9ffeb),
              borderRadius: BorderRadius.circular(10)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset(
                "assets/img/gojek.png",
                height: 60,
                width: 60,
              ),
              const SizedBox(
                height: 8,
              ),
              Text(
                "Digital Marketing",
                style: titleStyle(),
              ),
              const SizedBox(
                height: 5,
              ),
              Text(
                "Gojek - Jakarta",
                style: subTitle(),
              ),
              const SizedBox(
                height: 5,
              ),
              Row(
                children: [
                  Text(
                    "Full Time",
                    style: positionText(),
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  Text(
                    "Senior",
                    style: positionText(),
                  ),
                ],
              ),
            ],
          ),
        ),
        Container(
          padding: const EdgeInsets.all(8),
          decoration: BoxDecoration(
              color: Color(0xffffebe7),
              borderRadius: BorderRadius.circular(10)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset(
                "assets/img/shopee.png",
                height: 60,
                width: 60,
              ),
              const SizedBox(
                height: 8,
              ),
              Text(
                "Content Creator",
                style: titleStyle(),
              ),
              const SizedBox(
                height: 5,
              ),
              Text(
                "Shopee - Jakarta",
                style: subTitle(),
              ),
              const SizedBox(
                height: 5,
              ),
              Row(
                children: [
                  Text(
                    "Full Time",
                    style: positionText(),
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  Text(
                    "Internship",
                    style: positionText(),
                  ),
                ],
              ),
            ],
          ),
        ),
        Container(
          padding: const EdgeInsets.all(8),
          decoration: BoxDecoration(
              color: Color(0xffffe2eb),
              borderRadius: BorderRadius.circular(10)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset(
                "assets/img/bukalapak.png",
                height: 60,
                width: 60,
              ),
              const SizedBox(
                height: 8,
              ),
              Text(
                "Front End Dev",
                style: titleStyle(),
              ),
              const SizedBox(
                height: 5,
              ),
              Text(
                "Bukalapak - Jakarta",
                style: subTitle(),
              ),
              const SizedBox(
                height: 5,
              ),
              Row(
                children: [
                  Text(
                    "Full Time",
                    style: positionText(),
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  Text(
                    "Senior",
                    style: positionText(),
                  ),
                ],
              ),
            ],
          ),
        ),
        Container(
          padding: const EdgeInsets.all(8),
          decoration: BoxDecoration(
              color: Color(0xffe9f6ff),
              borderRadius: BorderRadius.circular(10)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset(
                "assets/img/blibli.png",
                height: 60,
                width: 60,
              ),
              const SizedBox(
                height: 8,
              ),
              Text(
                "UX Designer",
                style: titleStyle(),
              ),
              const SizedBox(
                height: 5,
              ),
              Text(
                "BliBli - Jakarta",
                style: subTitle(),
              ),
              const SizedBox(
                height: 5,
              ),
              Row(
                children: [
                  Text(
                    "Full Time",
                    style: positionText(),
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  Text(
                    "Remote",
                    style: positionText(),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    ),
  );
}

TextStyle positionText() {
  return TextStyle(color: Colors.grey, fontWeight: FontWeight.w400);
}

TextStyle subTitle() {
  return TextStyle(color: Colors.grey, fontWeight: FontWeight.w500);
}

TextStyle titleStyle() {
  return TextStyle(fontSize: 15, fontWeight: FontWeight.w700);
}
