import 'package:flutter/material.dart';

class DrawerScreen extends StatefulWidget {
  const DrawerScreen({Key? key}) : super(key: key);

  @override
  State<DrawerScreen> createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
      children: <Widget>[
        UserAccountsDrawerHeader(
          decoration: BoxDecoration(
            color: Color(0xff3e5089),
          ),
          accountName: Text("Mutia Marwa"),
          currentAccountPicture: CircleAvatar(
            backgroundImage: AssetImage("assets/img/pas_foto_mutia.jpg"),
          ),
          accountEmail: Text("mutiamarwa@gmail.com"),
        ),
        Padding(
            padding: const EdgeInsets.only(left: 20.0, top: 10, bottom: 10),
            child: Text("Accounts",
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.black54,
                )),
          ),
        DrawerListTile(
          iconData: Icons.lock,
          title: "Privacy and Security Settings",
          onTilePressed: () {},
        ),
        DrawerListTile(
          iconData: Icons.logout,
          title: "Log out",
          onTilePressed: () {},
        ),
        Divider(),
        Padding(
            padding: const EdgeInsets.only(left: 20.0, top: 10, bottom: 10),
            child: Text("General",
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.black54,
                )),
          ),
        DrawerListTile(
          iconData: Icons.notifications,
          title: "Notification Settings",
          onTilePressed: () {},
        ),
        DrawerListTile(
          iconData: Icons.language,
          title: "Language",
          onTilePressed: () {},
        ),
        Divider(),
        DrawerListTile(
          iconData: Icons.question_answer,
          title: "FAQ and Support",
          onTilePressed: () {},
        ),
        DrawerListTile(
          iconData: Icons.star_rate,
          title: "Rate Us",
          onTilePressed: () {},
        ),
        DrawerListTile(
          iconData: Icons.developer_board,
          title: "About Titian",
          onTilePressed: () {},
        ),
      ],
    ));
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData? iconData;
  final String? title;
  final VoidCallback? onTilePressed;

  const DrawerListTile({Key? key, this.iconData, this.title, this.onTilePressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title!,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}
