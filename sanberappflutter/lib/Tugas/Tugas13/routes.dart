import 'package:flutter/widgets.dart';
import 'package:sanberappflutter/Tugas/Tugas12/HomeScreen.dart';
import 'package:sanberappflutter/Tugas/Tugas12/LoginScreen.dart';
import 'package:sanberappflutter/Tugas/Tugas13/main_page.dart';

final Map<String, WidgetBuilder> routes = {
  LoginScreen.routeName: (context) => LoginScreen(),
  HomeScreen.routeName: (context) => HomeScreen(),
  MainPage.routeName: (context) => MainPage(),
};
