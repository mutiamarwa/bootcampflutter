import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sanberappflutter/Latihan/Latihan16/controller/appcontroller.dart';

class GetDataScreen extends StatelessWidget {
  const GetDataScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var controller = Get.put(AppController());
    return Scaffold(
      appBar: AppBar(title: Text("Get Data API")),
      body: Column(
        children: [
          Expanded(
            child: Obx(() {
              return controller.postloading.value
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : ListView.builder(
                      itemCount: controller.getpost.length,
                      itemBuilder: (context, index) {
                        var item = controller.getpost[index];
                        return Card(
                          child: ListTile(
                            title: Text(item.title.toString()),
                            subtitle: Text(item.body.toString()),
                            leading: Text(item.id.toString()),
                          ),
                        );
                      });
            }),
          ),
        ],
      ),
    );
  }
}
