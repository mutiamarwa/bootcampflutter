import 'package:get/get.dart';
import 'package:sanberappflutter/Latihan/Latihan16/services/services.dart';
import '../models/postmodel.dart';

class AppController extends GetxController {
  var getpost = <Postmodel>[].obs;
  Services services = Services();
  var postloading = true.obs;
  @override
  void onInit() {
    callpostmethod();
    super.onInit();
  }

  callpostmethod() async {
    try {
      postloading.value = true;
      var result = await services.getallpost();
      if (result != null) {
        getpost.assignAll(result);
      } else {
        print("null");
      }
    } finally {
      postloading.value = false;
    }
    update();
  }
}
