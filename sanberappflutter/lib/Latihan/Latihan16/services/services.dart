import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

import '../models/postmodel.dart';

class Services {
  Future<List<Postmodel>?> getallpost() async {
    try {
      var response = await http
          .get(Uri.parse("https://jsonplaceholder.typicode.com/posts"))
          .timeout(const Duration(seconds: 10), onTimeout: () {
        throw TimeoutException("Connection Time Out, Try again");
      });
      if (response.statusCode == 200) {
        List jsonresponse = convert.jsonDecode(response.body);
        return jsonresponse.map((e) => new Postmodel.fromJson(e)).toList();
      } else {
        return null;
      }
    } on TimeoutException catch (e) {
      print("response time out");
    }
  }
}
