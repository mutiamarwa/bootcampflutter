import 'package:flutter/material.dart';
import 'package:sanberappflutter/Quiz3/screen/home/model.dart';
import 'package:intl/intl.dart';

class Homescreen extends StatefulWidget {
  String? value;
  Homescreen({Key? key, this.value}) : super(key: key);

  @override
  _HomescreenState createState() => _HomescreenState(value);
}

class _HomescreenState extends State<Homescreen> {
  final Currency = new NumberFormat("#,##0", "id_ID");
  String? value;
  int total = 0;
  final jumlahdicart = List.generate(items.length, (index) => 0);

  _HomescreenState(this.value);
  @override
  void initState() {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(16),
        margin: EdgeInsets.only(top: 20),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(40.0),
                      child: Image.network(
                        "https://avatars.githubusercontent.com/u/52710807?v=4",
                        height: 80,
                        width: 80,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(value.toString()),
                  ],
                ),
                Row(
                  children: [
                    //## soal 4
                    //Tulis Coding disini
                    Text('Rp ${Currency.format(total)}'),

                    //sampai disini
                    SizedBox(
                      width: 10,
                    ),
                    Icon(Icons.shopping_cart)
                  ],
                ),
              ],
            ),
            SizedBox(height: 10),
            //#soal 3 silahkan buat coding disini
            //untuk container boleh di pake/dimodifikasi
            Container(
              height: MediaQuery.of(context).size.height / 1.5,
              child: GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 1,
                ),
                itemBuilder: (ctx, i) {
                  return Card(
                      clipBehavior: Clip.antiAlias,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Image.network(
                              items[i].profileUrl,
                              height: 70,
                              width: 100,
                            ),
                            // AspectRatio(
                            //   aspectRatio: 18.0 / 11.0,
                            //   child: NetworkImage(items[i].profileUrl!),
                            // ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text(items[i].name),
                                  const SizedBox(height: 4),
                                  Text('Rp ${Currency.format(items[i].price)}',
                                      style: TextStyle(
                                        color: Colors.grey[600],
                                      )),
                                  //const SizedBox(height: 4),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      if (jumlahdicart[i] == 0) ...[
                                        Text(''),
                                      ] else ...[
                                        IconButton(
                                          icon:
                                              Icon(Icons.remove_circle_outline),
                                          onPressed: () {
                                            setState(() {
                                              total -= items[i].price;
                                              jumlahdicart[i]--;
                                            });
                                          },
                                        ),
                                        Text(jumlahdicart[i].toString()),
                                      ],
                                      SizedBox(
                                        width: 2,
                                      ),
                                      IconButton(
                                        icon: Icon(Icons.add_shopping_cart),
                                        onPressed: () {
                                          setState(() {
                                            total += items[i].price;
                                            jumlahdicart[i]++;
                                          });
                                        },
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            )
                          ]));
                },
                itemCount: items.length,
              ), //silahkan dilanjutkan disini
            ),

            //sampai disini soal 3
          ],
        ),
      ),
    );
  }
}

Widget myWidget(BuildContext context) {
  return MediaQuery.removePadding(
    context: context,
    removeTop: true,
    child: GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
        ),
        itemCount: 300,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            color: Colors.amber,
            child: Center(child: Text('$index')),
          );
        }),
  );
}
