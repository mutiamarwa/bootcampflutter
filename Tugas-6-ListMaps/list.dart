//Return list of ascending/descending numbers from [startNum] to [finishNum]
List<int> range(startNum, finishNum) {
  List<int> list = [];
  if (startNum < finishNum) {
    for (var i = startNum; i <= finishNum; i++) {
      list.add(i);
    }
  } else if (startNum > finishNum) {
    for (var i = startNum; i >= finishNum; i--) {
      list.add(i);
    }
  } else {
    list.add(startNum);
  }
  return list;
}

//Return list of ascending/descending numbers from [startNum] to [finishNum] with [step]
rangeWithStep(startNum, finishNum, step) {
  List<int> list = [];
  if (step != 0) {
    if (startNum < finishNum) {
      for (var i = startNum; i <= finishNum; i += step) {
        list.add(i);
      }
    } else if (startNum > finishNum) {
      for (var i = startNum; i >= finishNum; i -= step) {
        list.add(i);
      }
    } else {
      list.add(startNum);
    }
    return list;
  } else {
    return "Step harus lebih besar dari 0!";
  }
}

//formatted print of datas in [list]
dataHandling(list) {
  //print(list.length);
  for (var item in list) {
    print("Nomor ID: ${item[0]}");
    print("Nama Lengkap: ${item[1]}");
    print("TTL: ${item[2]} ${item[3]}");
    print("Hobi: ${item[4]}\n");
  }
}

//Return flipped word from [kata]
balikKata(kata) {
  var flipped = [];
  int? panjangKata = kata.length.toInt();
  if (panjangKata != null) {
    for (var i = (panjangKata - 1); i >= 0; i--) {
      flipped.add(kata[i]);
    }
    return "${flipped.join()}";
  }
}

void main() {
  //No.1
  print(range(1, 10));
  print(range(11, 18));
  //print(range(21, 8));
  //print(range(5, 5));
  print('');

  //No.2
  print(rangeWithStep(1, 10, 2));
  print(rangeWithStep(11, 23, 3));
  print(rangeWithStep(5, 2, 1));
  //print(rangeWithStep(7, 7, 2));
  //print(rangeWithStep(5, 2, 0));
  print('');

  //No.3
  var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
  ];

  dataHandling(input);

  //No.4
  print(balikKata("Kasur"));
  print(balikKata("Sanbercode"));
  print(balikKata("Haji"));
  print(balikKata("racecar"));
  print(balikKata("Sanbers"));
}
