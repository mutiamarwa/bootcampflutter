void main() {
  print("Life");
  delayedPrint(2, "never flat").then((status) {
    print(status);
  });
  print("is");
}

///Delaying to print the [msg] for [sec] seconds
Future delayedPrint(int sec, String msg) {
  final duration = Duration(seconds: sec);
  return Future.delayed(duration).then((value) => msg);
}
