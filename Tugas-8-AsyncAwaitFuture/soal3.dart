void main() async {
  print("Ready. Sing");
  await line();
  await line2();
  await line3();
  await line4();
}

Future<void> line() async {
  String line = "pernahkan kau merasa";
  await Future.delayed(Duration(seconds: 5), () => (line),).then((value){
    print(value);
  });
}

Future<void> line2() async {
  String line = "pernahkah kau merasa.....";
  await Future.delayed(Duration(seconds: 3), () => (line),).then((value){
    print(value);
  });
}

Future<void> line3() async {
  String line = "pernahkan kau merasa";
  await Future.delayed(Duration(seconds: 2), () => (line),).then((value){
    print(value);
  });
}

Future<void> line4() async {
  String line = "Hatimu hampa, pernahkah kau merasa hati mu kosong....";
  await Future.delayed(Duration(seconds: 1), () => (line),).then((value){
    print(value);
  });
}

