import 'dart:io';

void main() {
  // No. 1 Ternary Operator Start
  print("Apakah anda ingin menginstal aplikasi ini?Y/T");
  String jawaban = stdin.readLineSync()!;

  // versi if
  // if (jawaban.toUpperCase() == 'Y') {
  //   print("Anda akan menginstall aplikasi dart");
  // } else if (jawaban.toUpperCase() == 'T') {
  //   print("Aborted");
  // } else {
  //   print("Jawaban tidak valid");
  // }

  // versi Ternary Operator
  (jawaban.toUpperCase() == 'Y')? print("Anda akan menginstall aplikasi dart"):
  (jawaban.toUpperCase() == 'T')? print("Aborted"):
  print("Jawaban tidak valid");

  /// No. 1 Ternary Operator Finish

  /// No. 2 If-else if dan else Start
  print("input nama:");
  String nama = stdin.readLineSync()!;
  print("input peran: (penyihir/guard/warewolf)");
  String peran = stdin.readLineSync()!;

  if (nama == '') {
    print("Nama harus diisi!");
  } else {
    if (peran == '') {
      print("Halo $nama, Pilih peranmu untuk memulai game!");
    } else {
      print("Selamat datang di Dunia Werewolf, $nama");
      if (peran.toLowerCase() == 'penyihir') {
        print(
            "Halo Penyihir $nama, kamu dapat melihat siapa yang menjadi warewolf!");
      } else if (peran.toLowerCase() == 'guard') {
        print(
            "Halo Guard $nama, kamu akan membantu melindungi temanmu dari serangan warewolf.");
      } else if (peran.toLowerCase() == 'warewolf') {
        print("Halo Warewolf $nama, Kamu akan memakan mangsa setiap malam");
      } else {
        print("Tidak ada peran tersebut $nama, silahkan isi kembali");
      }
    }
  }

  /// No. 2 If-else if dan else Finish

  /// No. 3 Switch case Start
  print("Halo, hari apa ini?");
  String inputHari = stdin.readLineSync()!;
  switch (inputHari.toLowerCase()) {
    case ('senin'):
      {
        print(
            "Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.");
        break;
      }
    case ('selasa'):
      {
        print(
            "Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.");
        break;
      }
    case ('rabu'):
      {
        print(
            "Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.");
        break;
      }
    case ('kamis'):
      {
        print(
            "Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.");
        break;
      }
    case ('jumat'):
      {
        print("Hidup tak selamanya tentang pacar.");
        break;
      }
    case ('sabtu'):
      {
        print("Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.");
        break;
      }
    case ('minggu'):
      {
        print(
            "Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.");
        break;
      }
    default:
      {
        print("Tidak ada hari tersebut dalam kamus saya");
      }
  }

  /// No. 3 Switch case Finish

  /// No. 4 Switch case Start
  var hari = 29;
  var bulan = 2;
  var tahun = 2000;

  if (hari<1 || hari>31){
    print("tanggal tidak valid");
  } else if (bulan<1 || bulan>12){
    print("bulan tidak valid");
  } else if (tahun<1900 || tahun>2200){
    print("tahun tidak valid");
  } else{
    switch (bulan) {
    case (1):
      {
        print("$hari Januari $tahun");
        break;
      }
    case (2):
      {
        if (( (tahun%4 == 0) && (tahun%100 != 0)) || (tahun%400 == 0)){
          if (hari>29){
          print("tanggal tidak valid");
          } else{
          print("$hari Februari $tahun");
          }
        } else {
          if (hari>28){
          print("tanggal tidak valid");
          } else{
          print("$hari Februari $tahun");
          }
        }
        break;
      }
    case (3):
      {
        print("$hari Maret $tahun");
        break;
      }
    case (4):
      {
        if (hari>30){
          print("tanggal tidak valid");
        } else{
          print("$hari April $tahun");
        }
        break;
      }
    case (5):
      {
        print("$hari Mei $tahun");
        break;
      }
    case (6):
      {
        if (hari>30){
          print("tanggal tidak valid");
        } else{
          print("$hari Juni $tahun");
        }
        break;
      }
    case (7):
      {
        print("$hari Juli $tahun");
        break;
      }
    case (8):
      {
        print("$hari Agustus $tahun");
        break;
      }
    case (9):
      {
        if (hari>30){
          print("tanggal tidak valid");
        } else{
          print("$hari September $tahun");
        }
        break;
      }
    case (10):
      {
          print("$hari Oktober $tahun");
        break;
      }
    case (11):
      {
        if (hari>30){
          print("tanggal tidak valid");
        } else{
          print("$hari November $tahun");
        }
        break;
      }
    case (12):
      {
          print("$hari Desember $tahun");
        break;
      }
   }
  }
  /// No. 4 Switch case Finish
}
