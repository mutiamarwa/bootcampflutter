teriak() {
  return "Halo Sanbers!";
}

///Returns the product of 2 numbers
kalikan(num1, num2) {
  return num1 * num2;
}


formatPerkenalan(name, age, address, hobby) {
  return "Nama saya $name, umur saya $age tahun, alamat saya di $address, dan saya punya hobby yaitu $hobby!";
}

///Returns the factorial of [x]
faktorial(x) {
  var hasil = 1;
  for (var i = 1; i <= x; i++) {
    hasil = hasil * i;
  }
  return hasil;
}

void main() {
  //No.1
  print(teriak());

  //No.2
  var num1 = 12;
  var num2 = 4;
  var hasilKali = kalikan(num1, num2);
  print(hasilKali);

  //No.3
  var name = "Agus";
  var age = 30;
  var address = "Jln. Malioboro, Yogyakarta";
  var hobby = "Gaming";
  var perkenalan = formatPerkenalan(name, age, address, hobby);
  print(perkenalan);

  //No.4
  print(faktorial(6));
}
