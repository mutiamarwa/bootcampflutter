import 'package:final_project/constant/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../constant/size_config.dart';
import '../screens/search_book_screen.dart';

class TextFieldDecoration extends StatelessWidget {
  String bookName = '';

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(15, 8, 0, 4),
      height: getProportionateScreenHeight(40),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        // border: Border.all(color: Colors.grey),
        color: Colors.grey[300],
      ),
      child: TextField(
          cursorColor: Colors.grey[800],
          onChanged: (value) {
            bookName = value;
          },
          decoration: InputDecoration(
            suffixIcon: GestureDetector(
              onTap: () {
                Get.to(SearchedBooks(), arguments: bookName);
              },
              child: Icon(
                Icons.search_rounded,
                color: Colors.grey[800],
                // size: 30,
              ),
            ),
            // focusColor: kPrimaryColor,
            // hoverColor: Colors.transparent,
            hintText: 'Search books ..',
            hintStyle: TextStyle(fontSize: 14),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent),
            ),

            // enabledBorder: OutlineInputBorder(
            //     borderSide: BorderSide(
            //       color: Colors.green,
            //     ),
            //     borderRadius: BorderRadius.circular(20)),
            // focusedBorder: OutlineInputBorder(
            //     borderSide: BorderSide(color: Color(0xFF3BBB6D)),
            //     borderRadius: BorderRadius.circular(20))),
          )),
    );
  }
}
