import 'package:final_project/controller/auth_controller.dart';
import 'package:final_project/controller/onboarding_controller.dart';
import 'package:final_project/controller/screen_controller.dart';

AuthController authController = AuthController.instance;
DashboardController dashboardController = DashboardController.instance;
OnBoardingController onBoardingController = OnBoardingController.instance;
// BookController bookController = BookController.instance;
