import 'package:final_project/constant/firebase.dart';
import 'package:final_project/controller/auth_controller.dart';
import 'package:final_project/controller/onboarding_controller.dart';
import 'package:final_project/controller/screen_controller.dart';
import 'package:final_project/screens/login_screen.dart';
import 'package:final_project/screens/dashboard.dart';
import 'package:final_project/screens/splash_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'constant/size_config.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await Firebase.initializeApp().then((value) => Get.put(AuthController()));
  await initialization.then((value) {
    Get.put(AuthController());
    Get.put(DashboardController());
    Get.put(OnBoardingController());
  });
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          //   primarySwatch: Colors.blue,
          //   appBarTheme: AppBarTheme(
          //   color: Colors.white,
          //   elevation: 0,
          //   // brightness: Brightness.light,
          //   iconTheme: IconThemeData(color: Colors.black),
          //   titleTextStyle: TextStyle(
          //     color: Color(0xFF8B8B8B),
          //     fontSize: 16,
          //   ),
          // ),
          ),
      home: SplashScreen(),
      // initialRoute: "/",
      // getPages: [
      //   GetPage(
      //     name: "/",
      //     page: () => Dashboard(),
      //     binding: DashboardBinding(),
      //   ),
      // ],
      // getPages: pageRouteApp.pages,
    );
  }
}
