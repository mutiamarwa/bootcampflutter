import 'package:final_project/constant/constants.dart';
import 'package:flutter/material.dart';

import '../models/book_model.dart';
import 'custom_button.dart';

class BookDetails extends StatelessWidget {
  final BookModel book;
  BookDetails({required this.book});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          book.title,
          style: TextStyle(
              fontFamily: 'Montserrat',
              fontWeight: FontWeight.w500,
              fontSize: 14),
        ),
        backgroundColor: kPrimaryColor,
      ),
      body: SafeArea(
          child: Padding(
        padding: const EdgeInsets.all(15),
        child: Center(
          child: ListView(children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 10,
                ),
                Container(
                  // decoration: BoxDecoration(
                  //   border: Border.all(color: kSecondaryColor),
                  // ),
                  child: Image.network(book.thumbnailUrl),
                  height: 150,
                ),
                SizedBox(height: 15),
                Container(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                      Stack(children: [
                        Text(
                          'Rating:',
                          style: TextStyle(fontStyle: FontStyle.italic),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 50.0),
                          child: Text(
                            '4.5/5',
                            style: TextStyle(fontStyle: FontStyle.italic),
                          ),
                        ),
                      ]),
                      SizedBox(height: 10),
                      Text(
                        book.title,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 10),
                      Text(
                        book.author,
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w500,
                            fontStyle: FontStyle.italic),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      // Text('Description',
                      //     textAlign: TextAlign.start,
                      //     style: TextStyle(
                      //       fontSize: 18,
                      //       fontWeight: FontWeight.w300,
                      //     )),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        book.description,
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.w300),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: ElevatedButton.icon(
                              icon: Icon(
                                Icons.bookmark,
                                color: Colors.grey[200],
                              ),
                              style: raisedButtonStyle_dark,
                              label: Text("Add to \nWishlist",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.grey[200])),
                              onPressed: () {},
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: ElevatedButton.icon(
                              icon: Icon(
                                Icons.swap_calls,
                                color: Colors.grey[200],
                              ),
                              style: raisedButtonStyle_dark,
                              label: Text("Swap",
                                  style: TextStyle(color: Colors.grey[200])),
                              onPressed: () {},
                            ),
                          ),
                        ],
                      ),
                    ]))
              ],
            ),
          ]),
        ),
      )),
    );
  }
}
