import 'package:final_project/constant/constants.dart';
import 'package:final_project/constant/controller.dart';
import 'package:final_project/constant/size_config.dart';
import 'package:final_project/controller/auth_controller.dart';
import 'package:final_project/screens/search_book_screen.dart';
import 'package:final_project/widgets/textfield_decoration.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart';

import '../models/book_dummy.dart';
import 'custom_button.dart';
import 'drawer_screen.dart';
import 'login_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);
  //GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    // FirebaseAuth auth = FirebaseAuth.instance;
    // var bookController = Get.put(BookController());
    String bookName = '';

    return Scaffold(
      //backgroundColor: Color(0xfffbfbfd),
      //key: _globalKey,
      // appBar: AppBar(
      //   backgroundColor: Colors.transparent,
      //   elevation: 0,
      //   leading: Builder(builder: (context) {
      //     return IconButton(
      //       onPressed: () {
      //         //_globalKey.currentState?.openDrawer();
      //         Scaffold.of(context).openDrawer();
      //       },
      //       tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
      //       icon: Icon(Icons.menu),
      //       color: Colors.grey,
      //     );
      //   }),
      // ),
      // drawer: DrawerScreen(),
      body: Center(
        child: Container(
          padding: EdgeInsets.all(getProportionateScreenHeight(25)),
          //margin: EdgeInsets.only(top: 40),
          color: Colors.white,
          child: ListView(
            physics: BouncingScrollPhysics(),
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      alignment: Alignment.centerLeft,
                      child: Obx(
                        () =>
                            Text('Hi, ${authController.userModel.value.name}'),
                      )),
                  Padding(
                    padding: const EdgeInsets.only(right: 8),
                    child: Icon(
                      Icons.notifications,
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
              // const SizedBox(
              //   height: 2,
              // ),
              Text(
                "Discover books to Swap!",
                style: TextStyle(
                    fontSize: getProportionateScreenHeight(23),
                    fontWeight: FontWeight.w800),
              ),
              const SizedBox(
                height: 15,
              ),
              TextFieldDecoration(),
              // const SizedBox(
              //   height: 15,
              // ),
              // genre_item(context),
              // Container(
              //   height: getProportionateScreenHeight(20),
              //   margin: EdgeInsets.only(top: 20),
              //   // padding: EdgeInsets.only(left: 25),
              //   child: DefaultTabController(
              //     length: 3,
              //     child: TabBar(
              //       labelPadding: EdgeInsets.all(0),
              //       indicatorPadding: EdgeInsets.all(0),
              //       isScrollable: true,
              //       labelColor: kPrimaryColor,
              //       indicatorColor: kPrimaryColor,
              //       tabs: [
              //         Tab(
              //           child: Container(
              //             margin: EdgeInsets.only(right: 23),
              //             child: Text('For You'),
              //           ),
              //         ),
              //         Tab(
              //           child: Container(
              //             margin: EdgeInsets.only(right: 23),
              //             child: Text('New'),
              //           ),
              //         ),
              //         Tab(
              //           child: Container(
              //             margin: EdgeInsets.only(right: 23),
              //             child: Text('Trending'),
              //           ),
              //         ),
              //       ],
              //     ),
              //   ),
              // ),
              SizedBox(
                height: getProportionateScreenHeight(15),
              ),

              SizedBox(
                height: getProportionateScreenHeight(15),
              ),
              Text(
                "Trending Now",
                style: TextStyle(
                    fontSize: getProportionateScreenHeight(18),
                    fontWeight: FontWeight.w700),
              ),
              Container(
                margin: EdgeInsets.only(top: 10),
                height: getProportionateScreenHeight(210),
                child: ListView.builder(
                  // padding: EdgeInsets.fromLTRB(25, 0, 6, 0),
                  itemCount: newbook.length,
                  physics: BouncingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Container(
                      margin: EdgeInsets.only(right: 14),
                      height: getProportionateScreenHeight(195),
                      width: getProportionateScreenWidth(120),
                      // child: Image.network(newbook[index]),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.grey,
                        image: DecorationImage(
                          image: NetworkImage(newbook[index]),
                          fit: BoxFit.cover,
                          // image: AssetImage(""),
                        ),
                      ),
                    );
                  },
                ),
              ),
              SizedBox(
                height: getProportionateScreenHeight(15),
              ),
              Text(
                "Books Around You",
                style: TextStyle(
                    fontSize: getProportionateScreenHeight(18),
                    fontWeight: FontWeight.w700),
              ),
              SizedBox(
                height: getProportionateScreenHeight(10),
              ),
              // jobs_item(context),
              ListView.builder(
                // scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: nearestBooks.length,
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.only(bottom: 19),
                    height: getProportionateScreenHeight(85),
                    // width: getProportionateScreenWidth(100),
                    color: Colors.white,
                    child: Row(
                      children: [
                        Container(
                          height: getProportionateScreenHeight(90),
                          width: getProportionateScreenWidth(62),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              image: DecorationImage(
                                image:
                                    NetworkImage(nearestBooks[index]["image"]!),
                                fit: BoxFit.cover,
                              )),
                        ),
                        SizedBox(
                          width: getProportionateScreenWidth(21),
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              nearestBooks[index]["title"]!,
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                            SizedBox(
                              height: getProportionateScreenHeight(5),
                            ),
                            Text(
                              nearestBooks[index]["author"]!,
                              style: TextStyle(color: Colors.grey),
                            ),
                          ],
                        ),
                      ],
                    ),
                  );
                },
              ),
              // Container(
              //   child: ElevatedButton(
              //     // onPressed: () {
              //     //   _signOut().then((value) =>
              //     //       Navigator.pushNamed(context, LoginScreen.routeName));
              //     // },
              //     onPressed: () {
              //       AuthController.instance.logOut();
              //     },
              //     child: Text("Logout"),
              //     style: raisedButtonStyle_dark,
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}

Container genre_item(BuildContext context) {
  // var bookController = Get.put(BookController());
  return Container(
    // height: MediaQuery.of(context).size.height / 2.2,
    child: GridView.count(
      primary: false,
      padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      crossAxisCount: 3,
      children: <Widget>[
        GestureDetector(
          onTap: () {
            // bookController.category.value = 'harry';
            // Books();
          },
          child: Container(
              padding: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                  color: Color(0xffe9ffeb),
                  borderRadius: BorderRadius.circular(10)),
              child: Text("Mysteri")),
        ),
        GestureDetector(
          onTap: () {
            // bookController.category.value = 'harry';
            // Books();
          },
          child: Container(
              padding: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                  color: Color(0xffe9ffeb),
                  borderRadius: BorderRadius.circular(10)),
              child: Text("Mysteri")),
        ),
        GestureDetector(
          onTap: () {
            // bookController.category.value = 'harry';
            // Books();
          },
          child: Container(
              padding: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                  color: Color(0xffe9ffeb),
                  borderRadius: BorderRadius.circular(10)),
              child: Text("Mysteri")),
        ),
      ],
    ),
  );
}

TextStyle positionText() {
  return TextStyle(color: Colors.grey, fontWeight: FontWeight.w400);
}

TextStyle subTitle() {
  return TextStyle(color: Colors.grey, fontWeight: FontWeight.w500);
}

TextStyle titleStyle() {
  return TextStyle(fontSize: 15, fontWeight: FontWeight.w700);
}
