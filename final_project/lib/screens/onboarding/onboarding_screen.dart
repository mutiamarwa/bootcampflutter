import 'package:final_project/constant/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../constant/size_config.dart';
import '../custom_button.dart';
import '../login_screen.dart';
import 'components/splash_content.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late PageController _pageController;
  int currentIndex = 0;
  List<Map<String, String>> splashData = [
    {
      "title": "Discover New Books",
      "text": "Discover collections of great books \nto read around you",
      "image": "assets/img/splash-1.png",
    },
    {
      "title": "Swap Books",
      "text":
          "Get the opportunity to read free books by \nswaping your old books with another user",
      "image": "assets/img/splash-2.png",
    },
    {
      "title": "Get Connected",
      "text": "Meet new friends and Build Local \nCommunity of Book Lovers",
      "image": "assets/img/splash-3.png",
    },
  ];

  static const _kDuration = const Duration(milliseconds: 300);
  static const _kCurve = Curves.ease;

  nextFunction() {
    _pageController.nextPage(duration: _kDuration, curve: _kCurve);
  }

  previousFunction() {
    _pageController.previousPage(duration: _kDuration, curve: _kCurve);
  }

  onChangedFunction(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: PageView(
                controller: _pageController,
                onPageChanged: onChangedFunction,
                children: <Widget>[
                  // Container(child: Center(child: Text("First Screen"))),
                  // Container(child: Center(child: Text("Second Screen"))),
                  // Container(child: Center(child: Text("Third Screen")))

                  SplashContent(
                    title: splashData[1]["title"]!,
                    text: splashData[1]["text"]!,
                    image: splashData[1]["image"]!,
                  ),
                  SplashContent(
                    title: splashData[2]["title"]!,
                    text: splashData[2]["text"]!,
                    image: splashData[2]["image"]!,
                  ),
                  SplashContent(
                    title: splashData[0]["title"]!,
                    text: splashData[0]["text"]!,
                    image: splashData[0]["image"]!,
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(20, 550, 20, 20),
            child: Row(
              // mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Indicator(
                  positionIndex: 0,
                  currentIndex: currentIndex,
                ),
                SizedBox(
                  width: 10,
                ),
                Indicator(
                  positionIndex: 1,
                  currentIndex: currentIndex,
                ),
                SizedBox(
                  width: 10,
                ),
                Indicator(
                  positionIndex: 2,
                  currentIndex: currentIndex,
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(20, 600, 20, 0),
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                // crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  if (currentIndex == 2) ...[
                    Container(
                      child: ElevatedButton(
                          style: raisedButtonStyle_dark,
                          child: Text("Get Started"),
                          onPressed: () {
                            Get.to(LoginScreen());
                          }),
                    ),
                  ] else ...[
                    Container(
                      child: Row(
                        children: [
                          TextButton(
                            onPressed: () {
                              // Get.to(LoginScreen());
                              // currentIndex = 2;
                              _pageController.jumpToPage(2);
                            },
                            child: Text(
                              "Skip",
                              style:
                                  TextStyle(fontSize: 15, color: kPrimaryColor),
                            ),
                          ),
                          Spacer(),
                          TextButton(
                            onPressed: () {
                              nextFunction();
                            },
                            child: Text(
                              "Next",
                              style:
                                  TextStyle(fontSize: 15, color: kPrimaryColor),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ]
                ],
              ),
            ),
          ),
          // Positioned(
          //   bottom: 30,
          //   left: 130,
          //   child: Padding(
          //     padding: const EdgeInsets.symmetric(horizontal: 20.0),
          //     child: Container(
          //       child: Row(
          //         mainAxisSize: MainAxisSize.max,
          //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //         children: <Widget>[
          //           InkWell(
          //               onTap: () => previousFunction(),
          //               child: Text("Previous")),
          //           SizedBox(
          //             width: 50,
          //           ),
          //           InkWell(onTap: () => nextFunction(), child: Text("Next"))
          //         ],
          //       ),
          //     ),
          //   ),
          // )
        ],
      ),
    );
  }
}

class Indicator extends StatelessWidget {
  final int positionIndex, currentIndex;
  const Indicator({required this.currentIndex, required this.positionIndex});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 6,
      width: positionIndex == currentIndex ? 20 : 6,
      decoration: BoxDecoration(
          border: Border.all(color: kPrimaryColor),
          color: positionIndex == currentIndex
              ? kPrimaryColor
              : Colors.transparent,
          borderRadius: BorderRadius.circular(5)),
    );
  }
}
