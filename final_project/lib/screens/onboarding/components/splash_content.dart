import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../constant/constants.dart';
import '../../../constant/size_config.dart';

class SplashContent extends StatelessWidget {
  const SplashContent({
    Key? key,
    required this.title,
    required this.text,
    required this.image,
  }) : super(key: key);

  final String text, image, title;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Spacer(
          flex: 2,
        ),

        Image.asset(
          image,
          height: getProportionateScreenHeight(345),
          width: getProportionateScreenWidth(315),
        ),
        // SvgPicture.asset(
        //   image,
        //   height: getProportionateScreenHeight(265),
        //   width: getProportionateScreenWidth(235),
        // ),
        // SizedBox(
        //   height: getProportionateScreenHeight(10),
        // ),
        Text(
          title,
          style: TextStyle(
            fontSize: getProportionateScreenWidth(25),
            color: kPrimaryColor,
            fontWeight: FontWeight.w600,
          ),
        ),
        Text(
          text,
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.grey[500]),
        ),
        Spacer(
          flex: 4,
        ),
      ],
    );
  }
}
