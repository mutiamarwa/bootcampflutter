import 'package:final_project/screens/search_book_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../widgets/reusable_card.dart';
import '../widgets/textfield_decoration.dart';

class Genres extends StatelessWidget {
  bool isClicked = false;
  String name = '';
  // BookServices service = BookServices();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF3BBB6D),
        leading: Icon(Icons.book),
        title: Text(
          'Select Genre',
          style:
              TextStyle(fontFamily: 'Montserrat', fontWeight: FontWeight.w500),
        ),
      ),
      body: SafeArea(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
              child: ListView(
            children: [
              SizedBox(
                height: 10,
              ),
              TextFieldDecoration(),
              GestureDetector(
                onTap: () {
                  // bookController.category.value = 'harry';
                  Get.to(SearchedBooks(), arguments: 'comedy');
                },
                child: Card(
                  margin:
                      EdgeInsets.only(left: 50, right: 50, top: 30, bottom: 20),
                  child: Padding(
                    padding: EdgeInsets.all(30.0),
                    child: Center(
                        child: Text(
                      'comedy',
                      style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.w400,
                          color: Colors.black),
                    )),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  // bookController.category.value = 'harry';
                  Get.to(SearchedBooks(), arguments: 'fiction');
                },
                child: Card(
                  margin:
                      EdgeInsets.only(left: 50, right: 50, top: 30, bottom: 20),
                  child: Padding(
                    padding: EdgeInsets.all(30.0),
                    child: Center(
                        child: Text(
                      'fiction',
                      style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.w400,
                          color: Colors.black),
                    )),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                child: GestureDetector(
                  onTap: () {
                    Get.back();
                  },
                  child: Text('Made With ❤ using Google Books API😉',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Montserrat')),
                ),
              )
            ],
          ))
        ],
      )),
    );
  }
}
