import 'package:final_project/constant/constants.dart';
import 'package:flutter/material.dart';

import '../constant/controller.dart';
import '../constant/size_config.dart';
import '../controller/auth_controller.dart';
import '../models/book_dummy.dart';
import '../widgets/numbers_widget.dart';
import '../widgets/profile_widget.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    final color = Colors.grey[200];
    return Scaffold(
      appBar: AppBar(
        // leading: Icon(Icons.arrow_back),
        centerTitle: true,
        title: Text("Profile",
            style:
                TextStyle(fontFamily: 'Montserrat', color: Colors.grey[800])),
        backgroundColor: Colors.white,
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.grey[800]),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.logout_rounded,
              color: Colors.grey[800],
            ),
            onPressed: () {
              // do something
              AuthController.instance.logOut();
            },
          )
        ],
      ),
      body: ListView(
        physics: BouncingScrollPhysics(),
        children: [
          ProfileWidget(
            imagePath: "assets/img/user_dummy.jpg",
            onClicked: () async {},
          ),
          const SizedBox(height: 20),
          buildName(),
          const SizedBox(height: 20),
          // Center(child: buildUpgradeButton()),
          // const SizedBox(height: 24),
          NumbersWidget(),
          const SizedBox(height: 18),
          Container(
            padding: const EdgeInsets.fromLTRB(25, 0, 25, 5),
            child: Row(
              children: [
                Text(
                  "Books Collections",
                  style: TextStyle(
                      fontSize: getProportionateScreenHeight(18),
                      fontWeight: FontWeight.w700),
                ),
                Spacer(),
                TextButton(
                  onPressed: () {},
                  child: Text(
                    "Edit",
                    style: TextStyle(fontSize: 15, color: kPrimaryColor),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(25, 0, 0, 0),
            height: getProportionateScreenHeight(210),
            child: ListView.builder(
              // padding: EdgeInsets.fromLTRB(25, 0, 6, 0),
              itemCount: newbook.length,
              physics: BouncingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return Container(
                  margin: EdgeInsets.only(right: 14),
                  height: getProportionateScreenHeight(195),
                  width: getProportionateScreenWidth(120),
                  // child: Image.network(newbook[index]),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.grey,
                    image: DecorationImage(
                      image: NetworkImage(newbook[index]),
                      fit: BoxFit.cover,
                      // image: AssetImage(""),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

Widget buildName() => Column(
      children: [
        Text(
          authController.userModel.value.name,
          style: TextStyle(fontWeight: FontWeight.w600, fontSize: 24),
        ),
        const SizedBox(height: 4),
        Text(
          authController.userModel.value.email,
          style: TextStyle(color: Colors.grey),
        )
      ],
    );

// Widget buildUpgradeButton() => ButtonWidget(
//       text: 'Edit Profile',
//       onClicked: () {},
//     );

// Widget buildAbout()=> Container(
//       padding: EdgeInsets.symmetric(horizontal: 48),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           Text(
//             'About',
//             style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
//           ),
//           const SizedBox(height: 16),
//           Text(
//             user.about,
//             style: TextStyle(fontSize: 16, height: 1.4),
//           ),
//         ],
//       ),
//     );
class ProfilePic extends StatelessWidget {
  const ProfilePic({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 115,
      width: 115,
      child: Stack(
        fit: StackFit.expand,
        clipBehavior: Clip.none,
        children: [
          CircleAvatar(
              backgroundImage: AssetImage("assets/img/user_dummy.jpg")),
          Positioned(
            right: -10,
            bottom: 0,
            child: SizedBox(
              height: 46,
              width: 46,
              child: ElevatedButton(
                onPressed: (() {}),
                style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.all(0),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)),
                    onPrimary: Colors.grey[700],
                    primary: Colors.grey[100]),
                child: Icon(
                  Icons.photo_camera,
                  size: 18,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
