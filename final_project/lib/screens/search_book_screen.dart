import 'dart:developer';

import 'package:final_project/constant/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../constant/size_config.dart';
import '../controller/search_book_controller.dart';
import '../models/book_model.dart';
import 'book_details.dart';

class SearchedBooks extends StatelessWidget {
  // final BookModel book;
  // SearchedBooks({required this.book});

  @override
  Widget build(BuildContext context) {
    var controller = Get.put(SearchedBookController());
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        title: Text(
          'Results for : ${Get.arguments}',
          style: TextStyle(
              fontWeight: FontWeight.w500, color: Colors.white, fontSize: 14),
        ),
      ),
      body: SafeArea(
          child: Column(
        children: [
          Expanded(
            child: Obx(() {
              return controller.isLoading.value
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : ListView.builder(
                      itemCount: controller.getbooks.length,
                      itemBuilder: (context, index) {
                        var item = controller.getbooks[index];
                        return Card(
                          child: ListTile(
                            title: Text(item.title, style: TextStyle()),
                            subtitle: Text(item.author,
                                style: TextStyle(color: Colors.grey)),
                            leading: Container(
                              height: getProportionateScreenHeight(90),
                              width: getProportionateScreenWidth(62),
                              child: Image.network(
                                item.thumbnailUrl,
                                // fit: BoxFit.cover,
                              ),
                            ),
                            onTap: () {
                              // _openDetailPage(context, item);
                              Get.to(
                                BookDetails(book: item),
                              );
                            },
                          ),
                        );
                      },
                    );
            }),
          )
        ],
      )),
    );
  }

  _openDetailPage(context, BookModel book) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => BookDetails(book: book)));
  }
}
