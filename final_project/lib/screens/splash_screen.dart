import 'package:flutter/material.dart';

import '../constant/size_config.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      width: double.maxFinite,
      height: double.maxFinite,
      color: Colors.white,
      child: Center(
        child: Container(
          width: 100,
          height: 100,
          child:
              // Image.asset("assets/img/logo_bukukita_big.png"),
              CircularProgressIndicator(
            backgroundColor: Color(0xff3e5089),
          ),
        ),
      ),
    );
  }
}
