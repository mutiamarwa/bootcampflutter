import 'package:flutter/material.dart';

final ButtonStyle raisedButtonStyle_dark = ElevatedButton.styleFrom(
  onPrimary: Colors.grey[300],
  primary: Color(0xff3e5089),
  //minimumSize: Size(88, 36),
  minimumSize: const Size.fromHeight(50),
  padding: EdgeInsets.symmetric(horizontal: 16),

  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(6)),
  ),
);

final ButtonStyle raisedButtonStyle_light = ElevatedButton.styleFrom(
  onPrimary: Color(0xff3e5089),
  primary: Colors.grey[300],
  //minimumSize: Size(88, 36),
  minimumSize: const Size.fromHeight(50),
  padding: EdgeInsets.symmetric(horizontal: 16),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(6)),
  ),
);
