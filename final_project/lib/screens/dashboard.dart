import 'package:final_project/controller/screen_controller.dart';
import 'package:final_project/screens/genre_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'home_screens.dart';
import 'save_screen.dart';
import 'chat_screen.dart';
import 'profile_screen.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<DashboardController>(
        init: DashboardController(),
        builder: (controller) {
          return Scaffold(
            body: SafeArea(
              child: IndexedStack(
                index: controller.tabIndex,
                children: [
                  HomeScreen(),
                  SaveScreen(),
                  // Genres(),
                  ChatScreen(),
                  ProfileScreen(),
                ],
              ),
            ),
            bottomNavigationBar: BottomNavigationBar(
                type: BottomNavigationBarType.fixed,
                currentIndex: controller.tabIndex,
                onTap: controller.changeTabIndex,
                selectedItemColor: Color(0xff3e5089),
                unselectedItemColor: Colors.grey,
                showSelectedLabels: true,
                showUnselectedLabels: false,
                elevation: 0,
                items: const <BottomNavigationBarItem>[
                  BottomNavigationBarItem(
                      icon: Icon(Icons.home), label: 'Home'),
                  BottomNavigationBarItem(
                      icon: Icon(Icons.bookmark), label: 'Wishlist'),
                  BottomNavigationBarItem(
                      icon: Icon(Icons.chat), label: 'Chat'),
                  BottomNavigationBarItem(
                      icon: Icon(Icons.person), label: 'Profile'),
                ]),
          );
        });
  }
}
