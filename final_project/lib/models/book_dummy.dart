List newbook = [
  "http://books.google.com/books/content?id=RLmxDwAAQBAJ&printsec=frontcover&img=1&zoom=5&source=gbs_api",
  "http://books.google.com/books/content?id=wmnuDwAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api",
  "http://books.google.com/books/content?id=eZ8wzgEACAAJ&printsec=frontcover&img=1&zoom=5&source=gbs_api",
  "http://books.google.com/books/content?id=1bm0DwAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api",
  "http://books.google.com/books/content?id=eZ8wzgEACAAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api",
];

List genre = ["Novel", "Self Development", "Biography", "Kids"];

List<Map<String, String>> nearestBooks = [
  {
    "title": "Keluar Dari Zona Nyaman",
    "author": "Amy Newmark",
    "image":
        "http://books.google.com/books/content?id=c2IFEAAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api",
  },
  {
    "title": "Man`s Search for Meaning",
    "author": "Viktor E. Frankl",
    "image":
        "http://books.google.com/books/content?id=8UNRDwAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api",
  },
  {
    "title": "Harry Potter dan Relikui Kematian",
    "author": "JK Rowling",
    "image":
        "http://books.google.com/books/content?id=3sSVzLsHIb8C&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api",
  },
  // {
  //   "title": "Atomic Habits",
  //   "author": "James Clear",
  //   "image":
  //       "http://books.google.com/books/content?id=1bm0DwAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api",
  // },
  // {
  //   "title": "Ugly Love - Wajah Buruk Cinta",
  //   "author": "Colleen Hoover",
  //   "image":
  //       "http://books.google.com/books/content?id=1sA8DwAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api",
  // },
];
