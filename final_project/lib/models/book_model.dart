import 'package:flutter/material.dart';
import 'dart:convert';

// List<BookModel> _parseBookJson(String str) =>
//     List<BookModel>.from(json.decode(str).map((x) => BookModel.fromJson(x)));
// String postmodelTojson(List<Postmodel> data) =>
//     json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class BookModel {
  BookModel(
      {required this.title,
      required this.author,
      required this.thumbnailUrl,
      required this.description})
      : assert(title != null);

  String title;
  String author;
  String thumbnailUrl;
  String description;

  factory BookModel.fromJson(Map<String, dynamic> json) {
    String getAuthor() {
      if (json['volumeInfo']['authors'] != null) {
        return (json['volumeInfo']['authors'] as List).join(', ');
      } else {
        return '';
      }
    }

    String getDescription() {
      if (json['volumeInfo']['description'] != null) {
        return json['volumeInfo']['description'];
      } else {
        return '';
      }
    }

    String getThumbnailSafety() {
      final imageLinks = json['volumeInfo']['imageLinks'];
      if (imageLinks != null && imageLinks['thumbnail'] != null) {
        return imageLinks['thumbnail'];
      } else {
        return "https://yt3.ggpht.com/ytc/AKedOLR0Q2jl80Ke4FS0WrTjciAu_w6WETLlI0HmzPa4jg=s176-c-k-c0x00ffffff-no-rj";
      }
    }

    return BookModel(
      title: json['volumeInfo']['title'],
      author: getAuthor(),
      // description: json['volumeInfo']['description'],
      description: getDescription(),
      // thumbnailUrl: json['volumeInfo']['imageLinks']['smallThumbnail']
      thumbnailUrl: getThumbnailSafety().replaceAll("http", "https"),
    );
  }

  // Map<String, dynamic> toJson() => {

  // };

  // assert(description != null);
}
