import 'package:final_project/constant/firebase.dart';
import 'package:final_project/screens/home_screens.dart';
import 'package:final_project/screens/login_screen.dart';
import 'package:final_project/screens/dashboard.dart';
import 'package:final_project/screens/onboarding/onboarding_screen.dart';
import 'package:final_project/screens/onboarding/onboarding_screen.dart';
import 'package:final_project/screens/splash_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../models/user.dart';

class AuthController extends GetxController {
  static AuthController instance = Get.find();
  FirebaseAuth auth = FirebaseAuth.instance;
  late Rx<User?> _user;
  // RxBool isLoggedIn = false.obs;
  TextEditingController name = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  String usersCollection = "users";
  Rx<UserModel> userModel = UserModel(email: '', id: '', name: '').obs;

  @override
  void onReady() {
    super.onReady();
    _user = Rx<User?>(auth.currentUser);
    _user.bindStream(auth.userChanges());
    ever(_user, _initialScreen);
  }

  _initialScreen(User? user) {
    if (user == null) {
      // Get.offAll(() => OnBoardingScreen());
      Get.offAll(() => MyHomePage());
    } else {
      userModel.bindStream(listenToUser());
      Get.offAll(() => Dashboard());
    }
  }

  void register() async {
    try {
      await auth
          .createUserWithEmailAndPassword(
              email: email.text.trim(), password: password.text.trim())
          .then((result) {
        String _userId = result.user!.uid;
        _addUserToFirestore(_userId);
        _clearControllers();
      });
    } on FirebaseAuthException catch (e) {
      Get.snackbar(
        "About User",
        "User mesage",
        backgroundColor: Colors.orangeAccent,
        snackPosition: SnackPosition.BOTTOM,
        titleText: Text(
          "Account creation failed",
          style: TextStyle(color: Colors.white),
        ),
        messageText: Text(
          e.toString(),
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      );
    }
    ;
  }

  void login() async {
    try {
      await auth
          .signInWithEmailAndPassword(
              email: email.text.trim(), password: password.text.trim())
          .then((result) {
        _clearControllers();
      });
    } on FirebaseAuthException catch (e) {
      Get.snackbar(
        "About Login",
        "Login message",
        backgroundColor: Colors.orangeAccent,
        snackPosition: SnackPosition.BOTTOM,
        titleText: Text(
          "Login Failed, Try Again",
          // style: TextStyle(color: Colors.white),
        ),
        messageText: Text(
          e.message!,
          style: TextStyle(
              // color: Colors.white,
              ),
        ),
      );
    }
  }

  void logOut() async {
    await auth.signOut();
  }

  _addUserToFirestore(String userId) {
    firebaseFirestore.collection(usersCollection).doc(userId).set({
      "name": name.text.trim(),
      "id": userId,
      "email": email.text.trim(),
      "cart": []
    });
  }

  _clearControllers() {
    name.clear();
    email.clear();
    password.clear();
  }

  updateUserData(Map<String, dynamic> data) {
    // logger.i("UPDATED");
    firebaseFirestore
        .collection(usersCollection)
        .doc(_user.value!.uid)
        .update(data);
  }

  Stream<UserModel> listenToUser() => firebaseFirestore
      .collection(usersCollection)
      .doc(_user.value!.uid)
      .snapshots()
      .map((snapshot) => UserModel.fromSnapshot(snapshot));
}
