import 'package:final_project/screens/home_screens.dart';
import 'package:get/get.dart';

class DashboardBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DashboardController>(() => DashboardController());
    Get.lazyPut<HomeScreenController>(() => HomeScreenController());
    Get.lazyPut<SaveScreenController>(() => SaveScreenController());
    Get.lazyPut<ChatScreenController>(() => ChatScreenController());
    Get.lazyPut<ProfileScreenController>(() => ProfileScreenController());
  }
}

class DashboardController extends GetxController {
  static DashboardController instance = Get.find();
  var tabIndex = 0;
  void changeTabIndex(int index) {
    tabIndex = index;
    update();
  }
}

class HomeScreenController extends GetxController {}

class SaveScreenController extends GetxController {}

class ChatScreenController extends GetxController {}

class ProfileScreenController extends GetxController {}
