import 'package:get/get.dart';

class OnBoardingController extends GetxController {
  static OnBoardingController instance = Get.find();
  RxInt index = 0.obs;
  void NextIndex() {
    if (index == 2) {
      index.value = 0;
    } else {
      index++;
    }
    update();
  }

  void GetIndex(int value) {
    index.value = value;
  }
}
