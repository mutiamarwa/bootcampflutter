void main() {
// No.1 Looping While Start
// Loop 1
  var count1 = 2;
  print("LOOPING PERTAMA");
  while (count1 <= 20) {
    print("$count1 - I love coding");
    count1 = count1 + 2;
  }

// Loop 2
  var count2 = 20;
  print("LOOPING KEDUA");
  while (count2 >= 2) {
    print("$count2 - I will become a mobile developer");
    count2 = count2 - 2;
  }
// No.1 Looping While Finish

// No.2 Looping for Start
  for (var i = 1; i <= 20; i++) {
    if ((i % 3 == 0) && (i % 2 == 1)) {
      print("$i - I Love Coding ");
    } else if ((i % 2 == 1)) {
      print("$i - Santai ");
    } else {
      print("$i - Berkualitas ");
    }
  }
// No.2 Looping for Finish

// No.3 Membuat persegi panjang Start
  var col = 8;
  var row = 4;
  String barispersegi = '';
  print("persegi panjang $col x $row :");
  for (var i = 1; i <= row; i++) {
    for (var j = 1; j <= col; j++) {
      barispersegi = barispersegi + '#';
    }
    print(barispersegi);
    barispersegi = '';
  }
// No.3 Membuat persegi panjang Finish

// No.4 Membuat Tangga Start
  var alas = 7;
  var tinggi = 7;
  String baristangga = '';
  print("tangga dengan dengan tinggi $tinggi dan alas $alas :");
  for (var i = 1; i <= tinggi; i++) {
    for (var j = 1; j <= i; j++) {
      baristangga = baristangga + '#';
    }
    print(baristangga);
    baristangga = '';
  }
// No.4 Membuat Tangga Finish
}
