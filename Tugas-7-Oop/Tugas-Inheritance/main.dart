import 'armor_titan.dart';
import 'titan.dart';
import 'armor_titan.dart';
import 'attack_titan.dart';
import 'beast_titan.dart';
import 'human.dart';

void main(){
  ArmorTitan armortitan = ArmorTitan();
  AttackTitan attacktitan = AttackTitan();
  BeastTitan beasttitan = BeastTitan();
  Human human = Human();

  armortitan.powerPoint = 10;
  attacktitan.powerPoint = 8;
  beasttitan.powerPoint = 20;
  human.powerPoint = 3;

  print("power point Armor Titan : ${armortitan.powerPoint}");
  print("power point Attack Titan : ${attacktitan.powerPoint}");
  print("power point Beast Titan : ${beasttitan.powerPoint}");
  print("power point Human : ${human.powerPoint}");

  print("serangan dari Armor Titan : " + armortitan.terjang());
  print("serangan dari Attack Titan : " + attacktitan.punch());
  print("serangan dari Beast Titan : " + beasttitan.lempar());
  print("serangan dari Human : " + human.killAllTitan());
}