class Titan {
  late int _powerPoint;

  int get powerPoint => _powerPoint;
  set powerPoint(int value) {
    ///Validate the value to minimum 5 points
    if (value <= 5) {
      value = 5;
    }
    _powerPoint = value;
  }
}
