import 'dart:math';

class Lingkaran {
  late double _radius;
  final double pi = 22/7;

  set Radius(double value) {
    ///Validate the value to always be positive numbers
    if (value < 0) {
      value *= -1;
    }
    _radius = value;
  }

  double get Radius => _radius;

  double hitungLuas() {
    return pow(this._radius, 2) * pi;
  }
}
