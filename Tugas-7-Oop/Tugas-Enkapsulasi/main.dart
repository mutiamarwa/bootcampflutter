import 'lingkaran.dart';

void main() {
  Lingkaran lingkaran1 = new Lingkaran();
  double luasLingkaran;

  lingkaran1.Radius = 7;
  luasLingkaran = lingkaran1.hitungLuas();
  print(luasLingkaran);

  Lingkaran lingkaran2 = new Lingkaran();
  lingkaran2.Radius = 10;
  print("Luas lingkaran dengan ${lingkaran2.Radius} adalah ${lingkaran2.hitungLuas()}");

  Lingkaran lingkaran3 = new Lingkaran();
  lingkaran3.Radius = -14;
  print("Luas lingkaran dengan ${lingkaran3.Radius} adalah ${lingkaran3.hitungLuas()}");
}
