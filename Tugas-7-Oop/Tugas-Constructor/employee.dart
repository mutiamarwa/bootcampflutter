class Employee {
  late String id;
  late String name;
  late String department;

  Employee(String this.id, String this.name, String this.department);
}
