import 'employee.dart';

void main() {
  var pekerja1 = new Employee('001', 'Asah Asih Asuh', 'Research and Development');
  var pekerja2 = new Employee('002', 'Budi Pekerti', 'Finance');
  var pekerja3 = new Employee('003', 'Cari Cara', 'Project Management');

  print("${pekerja1.name} dengan no.ID ${pekerja1.id} bekerja di bagian ${pekerja1.department}.");
  print("${pekerja2.name} dengan no.ID ${pekerja2.id} bekerja di bagian ${pekerja2.department}.");
  print("${pekerja3.name} dengan no.ID ${pekerja3.id} bekerja di bagian ${pekerja3.department}.");
}
