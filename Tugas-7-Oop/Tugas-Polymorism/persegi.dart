import 'bangun_datar.dart';

class Persegi extends BangunDatar {
  late double sisi;

  ///Initialize the value of the object
  Persegi(double sisi) {
    this.sisi = sisi;
  }

  @override
  double hitungLuas() {
    return sisi*sisi;
  }

  @override
  double hitungKeliling() {
    return sisi*4;
  }
}