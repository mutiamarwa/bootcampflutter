import 'bangun_datar.dart';

class Lingkaran extends BangunDatar {
  late double radius;
  final double pi = 22 / 7;

  ///Initialize the value of the object
  Lingkaran(double radius) {
    this.radius = radius;
  }

  @override
  double hitungLuas() {
    return radius * radius * pi;
  }

  @override
  double hitungKeliling() {
    return radius * 2 * pi;
  }
}
