import 'bangun_datar.dart';
import 'dart:math';

class Segitiga extends BangunDatar {
  late double alas;
  late double tinggi;
  late double sisimiring;

  ///Initialize the value of the object
  Segitiga(double alas, double tinggi) {
    this.alas = alas;
    this.tinggi = tinggi;
    ///Get [sisimiring] by phytagoras formula
    this.sisimiring = sqrt(pow(alas,2) + pow(tinggi,2));
  }

  @override
  double hitungLuas() {
    return alas * tinggi * 0.5;
  }

  @override
  double hitungKeliling() {
    return alas + tinggi + sisimiring;
  }
}
