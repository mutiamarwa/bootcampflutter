import 'bangun_datar.dart';
import 'lingkaran.dart';
import 'persegi.dart';
import 'segitiga.dart';

void main() {
  BangunDatar bangunDatar = new BangunDatar();
  Lingkaran lingkaran = new Lingkaran(7);
  Persegi persegi = new Persegi(5);
  Segitiga segitiga = new Segitiga(3, 4);

  bangunDatar.hitungLuas();
  bangunDatar.hitungKeliling();

  print("Luas lingkaran : ${lingkaran.hitungLuas()}");
  print("Keliling lingkaran : ${lingkaran.hitungKeliling()}");

  print("Luas persegi : ${persegi.hitungLuas()}");
  print("Keliling persegi : ${persegi.hitungKeliling()}");

  print("Luas segitiga : ${segitiga.hitungLuas()}");
  print("Keliling segitiga : ${segitiga.hitungKeliling()}");
}
