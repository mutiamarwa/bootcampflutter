void main() {
  Segitiga segitiga1 = new Segitiga();
  double luasKotak;
  segitiga1.alas = 20.0;
  segitiga1.tinggi = 30.0;
  luasKotak = segitiga1.hitungLuas();
  print("$luasKotak");
  //print("Luas segitiga dengan alas ${segitiga1.alas} & tinggi ${segitiga1.tinggi} adalah ${segitiga1.hitungLuas()}");

  Segitiga segitiga2 = new Segitiga();
  segitiga2.alas = 10;
  segitiga2.tinggi = 20;
  print("Luas segitiga dengan alas ${segitiga2.alas} & tinggi ${segitiga2.tinggi} adalah ${segitiga2.hitungLuas()}");
  //print("${segitiga2.hitungLuas()}");
}

class Segitiga {
  late double alas;
  late double tinggi;

  double hitungLuas() {
    return this.alas * tinggi * 0.5;
  }
}
